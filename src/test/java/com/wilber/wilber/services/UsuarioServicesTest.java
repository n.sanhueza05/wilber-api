package com.wilber.wilber.services;

import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.repositories.UsuarioRepository;
import com.wilber.wilber.utils.JwtUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class UsuarioServicesTest {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UsuarioServices usuarioServices;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private EmailServices emailServices;



    @BeforeEach
    void setUp() {

    }


    @Test
    @DisplayName("Usuario registrado correctamente")
    void userRegister() throws Exception {
        Usuario userTest = new Usuario();
        userTest.setId((long)1);
        userTest.setNombreUsuario("test");
        userTest.setEmail("correoTest");
        userTest.setPassword("password");

        String expected = "Registrado correctamente";
        String actual = usuarioServices.userRegister(userTest);

        Optional<Usuario> usuarioBD = usuarioRepository.findById(1L);

        // Falla porque no se guarda en BD
        if (usuarioBD.isEmpty()) fail();

        // Usuario BD no es nulo
        assertNotNull(usuarioBD.get());

        // ID corresponde
        assertEquals(1L, usuarioBD.get().getId());

        // Se encripte la password
        System.out.println(usuarioBD.get().getPassword());
        assertNotEquals("password",usuarioBD.get().getPassword());

        // Verificar respuesta Metodo
        assertEquals(expected,actual);

    }

    @Test
    @DisplayName("Usuario correo utilizado")
    void userRegisterCorreoYaUtilizado() throws Exception {
        Usuario userTest1 = new Usuario();
        userTest1.setId(100L);
        userTest1.setEmail("correotestrepetido");
        Usuario u = usuarioRepository.save(userTest1);

        Usuario userTest = new Usuario();
        userTest.setNombreUsuario("test");
        userTest.setEmail("correoTestRepetido");
        userTest.setPassword("password");

        String expected = "Email en Uso";
        String actual = usuarioServices.userRegister(userTest);

        // Verificar respuesta Metodo
        assertEquals(expected,actual);
    }



}