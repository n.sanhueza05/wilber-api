package com.wilber.wilber.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.services.UsuarioServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class UsuarioControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UsuarioServices usuarioServices;

    @Autowired
    private UsuarioController usuarioController;


    private Usuario userTest = new Usuario();

    @BeforeEach
    void setUp() {
        userTest = new Usuario();
        userTest.setId((long)1);
        userTest.setNombreUsuario("test");
        userTest.setEmail("correoTest");
        userTest.setPassword("password");
    }

    @Test
    @DisplayName("Usuario logeado con exito")
    void login() throws Exception {

        when(usuarioServices.logearEmailPassword(userTest)).thenReturn("token");

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(userTest);


        MockHttpServletResponse response = mvc.perform(post("/api/user/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))

                .andReturn()
                .getResponse();

        assertEquals(HttpStatus.OK.value(),response.getStatus());
        assertEquals("token",response.getContentAsString());
    }

    @Test
    @DisplayName("Usuario no se encuentra en la BD")
    void loginErrorNoSeEncuentraUsuario() throws Exception {

        when(usuarioServices.logearEmailPassword(userTest)).thenReturn("Usuario inexistente");

        String expected = "Usuario inexistente";

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(userTest);

        MockHttpServletResponse response = mvc.perform(post("/api/user/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))

                .andReturn()
                .getResponse();

        // Testing status HTTP
        assertEquals(HttpStatus.OK.value(),response.getStatus());

        // Testing Respuesta esperada
        assertEquals(expected,response.getContentAsString());
    }

    @Test
    @DisplayName("Usuario no Activado")
    void loginErrorNoActivado() throws Exception {

        when(usuarioServices.logearEmailPassword(userTest)).thenReturn("Usuario no activado");

        String expected = "Usuario no activado";

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(userTest);

        MockHttpServletResponse response = mvc.perform(post("/api/user/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))

                .andReturn()
                .getResponse();

        // Testing status HTTP
        assertEquals(HttpStatus.OK.value(),response.getStatus());

        // Testing Respuesta esperada
        assertEquals(expected,response.getContentAsString());
    }

}