package com.wilber.wilber.controllers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class DefaultControllerTest {

    @Autowired
    private MockMvc mvc;


    @Test
    @DisplayName("Verificar API Funcional")
    void defaultMethod() throws Exception {
        MockHttpServletResponse response = mvc.perform(get("/")).andReturn().getResponse();
        String expected ="<h1>API WILBER</h1>";

        assertEquals(expected,response.getContentAsString());
    }
}