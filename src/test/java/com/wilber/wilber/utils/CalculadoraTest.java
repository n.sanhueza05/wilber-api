package com.wilber.wilber.utils;

import com.wilber.wilber.models.Asignatura;
import com.wilber.wilber.models.AsignaturaInscrita;
import com.wilber.wilber.models.Nota;
import com.wilber.wilber.models.Usuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class CalculadoraTest {

    private Calculadora calculadora;

    private AsignaturaInscrita asignaturaInscrita;


    @BeforeEach
    void setUp() {
        calculadora = new Calculadora();
        Asignatura asignatura = new Asignatura();
        asignatura.setNombre("Estadistica");
        asignaturaInscrita = new AsignaturaInscrita();
        asignaturaInscrita.setPorTeorico(50);
        asignaturaInscrita.setPorPractico(50);
        asignaturaInscrita.setEliminado(false);
        asignaturaInscrita.setApruebanJuntos(true);
        asignaturaInscrita.setAsignatura(asignatura);
    }


    @Test
    @DisplayName("Promedio de alumno")
    void averageFromUser() {
        Usuario u = new Usuario();
        List<AsignaturaInscrita> inscritasTest = new ArrayList<>();
        float expected = 42.5F;
        for (int i = 0; i < 3; i++) {
            List<Nota> notasTest = new ArrayList<>();
            Nota n1 = new Nota();
            n1.setValorNota(40);
            n1.setConocida(true);
            Nota n2 = new Nota();
            n2.setValorNota(45);
            n2.setConocida(true);
            Nota n3 = new Nota();
            n3.setValorNota(60);
            n3.setConocida(false);
            notasTest.add(n1);
            notasTest.add(n2);
            notasTest.add(n3);
            AsignaturaInscrita inscritaTest = new AsignaturaInscrita();
            inscritaTest.setNotas(notasTest);
            inscritasTest.add(inscritaTest);
        }
        u.setAsignaturasInscritas(inscritasTest);
        float actual = calculadora.averageFromUser(u);
        System.out.println("-------------ACTUAL-------------");
        System.out.println(actual);
        Assertions.assertEquals(expected,actual);
    }

    @Test
    void averageAsignaturaInscrita() {
        List<Nota> notasTest = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Nota n1 = new Nota();
            n1.setValorNota(20);
            n1.setConocida(true);
            Nota n2 = new Nota();
            n2.setValorNota(45);
            n2.setConocida(true);
            Nota n3 = new Nota();
            n3.setValorNota(60);
            n3.setConocida(false);
            notasTest.add(n1);
            notasTest.add(n2);
            notasTest.add(n3);
        }
        asignaturaInscrita.setNotas(notasTest);
        float actual = calculadora.averageAsignaturaInscrita(asignaturaInscrita);
        float expected = 32.5f;
        float unExpected = 10.0f;

        System.out.println("----------------------------ACTUAL----------------------------");
        System.out.println(actual);
        System.out.println("--------------------------------------------------------------");
        System.out.println("---------------------------EXPECTED---------------------------");
        System.out.println(expected);
        System.out.println("--------------------------------------------------------------");
        assertEquals(expected,actual);
        assertNotEquals(unExpected,actual);
    }

    @Test
    @DisplayName("Calcular promedio de asignaturas inscritas")
    void averagesAsignaturasInscritas() {
        List<Float> promedios = new ArrayList<>();
        promedios.add(40.0f);
        promedios.add(50.0f);
        promedios.add(65.0f);
        promedios.add(30.0f);
        float expected = 46.25f;
        float actual = calculadora.averagesAsignaturasInscritas(promedios);

        System.out.println("----------------------------ACTUAL----------------------------");
        System.out.println(actual);
        System.out.println("--------------------------------------------------------------");
        System.out.println("---------------------------EXPECTED---------------------------");
        System.out.println(expected);
        System.out.println("--------------------------------------------------------------");
        assertEquals(expected,actual);
    }


    //---------------------------------- Asignaturas T - P Aprueba Separado --------------------------------//

    @Test
    @DisplayName("Asignatura Aprueban Separado  | Asignatura Aprobable ")
    void getGradesFromAsignaturaSeparados() {
        List<NotaCalculator> notas = new ArrayList<>();
        NotaCalculator n1 = new NotaCalculator();
        n1.setId(1);
        n1.setNombre("Nota 1");
        n1.setConocida(true);
        n1.setTeorico(true);
        n1.setValor(30);
        n1.setPorcentaje(30);

        NotaCalculator n2 = new NotaCalculator();
        n2.setId(2);
        n2.setNombre("Nota 2");
        n2.setTeorico(true);
        n2.setConocida(false);
        n2.setValor(10);
        n2.setPorcentaje(70);

        NotaCalculator n3 = new NotaCalculator();
        n3.setId(3);
        n3.setNombre("Nota 3");
        n3.setTeorico(false);
        n3.setConocida(false);
        n3.setValor(10);
        n3.setPorcentaje(100);

        notas.add(n1); notas.add(n2); notas.add(n3);

        Map<String,Object> actual = calculadora.getGradesFromAsignatura(notas,50,50,false);

        Map<String,Object> expected = new HashMap<>();

        expected.put("notasConocidas",getNotasConocidas());
        expected.put("message","Aprobable");
        expected.put("notasNoConocidas",getNotasNoConocidas());

        actual.forEach((key,value) -> {
            System.out.println("--------------"+key+"---------------");
            if (Objects.equals(key, "message")){
                System.out.println(value);
            }else{
                List<NotaCalculator> notasResponse = (List<NotaCalculator>) value;
                notasResponse.forEach( n ->{
                    System.out.println(n.getNombre() + " " + n.getValor());
                });
            }
        });
        // Mensaje de que ramo es aprobable
        Assertions.assertEquals(expected.get("message"),actual.get("message"));

        List<NotaCalculator> notasConocidasActual = (List<NotaCalculator>) actual.get("notasConocidas");
        List<NotaCalculator> notasConocidasExpected = (List<NotaCalculator>) getNotasConocidasSeparadas();

        List<NotaCalculator> notasNoConocidasActual = (List<NotaCalculator>) actual.get("notasNoConocidas");
        List<NotaCalculator> notasNoConocidasExpected = (List<NotaCalculator>) getNotasNoConocidasSeparadas();

        //Verificar valor de cada nota conocida se mantenga
        for (int i = 0; i < notasConocidasActual.size(); i++) {
            Assertions.assertEquals(notasConocidasExpected.get(i).getValor(),notasConocidasActual.get(i).getValor());
        }

        //Verificar valor de cada nota no conocida corresponda para que promedio sea 39.5
        for (int i = 0; i < notasNoConocidasActual.size(); i++) {
            Assertions.assertEquals(notasNoConocidasExpected.get(i).getValor(),notasNoConocidasActual.get(i).getValor());
        }

    }

    @Test
    @DisplayName("Asignatura Aprueban Separado  | Asignatura NO Aprobable ")
    void getGradesFromAsignaturaSeparadosNoAprobable() {
        List<NotaCalculator> notas = new ArrayList<>();
        NotaCalculator n1 = new NotaCalculator();
        n1.setId(1);
        n1.setNombre("Nota 1");
        n1.setConocida(true);
        n1.setTeorico(true);
        n1.setValor(10);
        n1.setPorcentaje(30);

        NotaCalculator n2 = new NotaCalculator();
        n2.setId(2);
        n2.setNombre("Nota 2");
        n2.setTeorico(true);
        n2.setConocida(true);
        n2.setValor(10);
        n2.setPorcentaje(70);

        NotaCalculator n3 = new NotaCalculator();
        n3.setId(3);
        n3.setNombre("Nota 3");
        n3.setTeorico(false);
        n3.setConocida(false);
        n3.setValor(10);
        n3.setPorcentaje(100);

        notas.add(n1); notas.add(n2); notas.add(n3);

        Map<String,Object> actual = calculadora.getGradesFromAsignatura(notas,50,50,false);

        Map<String,Object> expected = new HashMap<>();

        expected.put("notasConocidas",getNotasConocidas());
        expected.put("message","No Aprobable");
        expected.put("notasNoConocidas",getNotasNoConocidas());

        actual.forEach((key,value) -> {
            System.out.println("--------------"+key+"---------------");
            if (Objects.equals(key, "message")){
                System.out.println(value);
            }else{
                List<NotaCalculator> notasResponse = (List<NotaCalculator>) value;
                notasResponse.forEach( n ->{
                    System.out.println(n.getNombre() + " " + n.getValor());
                });
            }
        });
        // Mensaje de que ramo es aprobable
        Assertions.assertEquals(expected.get("message"),actual.get("message"));
    }


    //---------------------------------- Asignaturas T - P Aprueba Junto --------------------------------//

    @Test
    @DisplayName("Asignatura Aprueban Juntos  | Asignatura Aprobable ")
    void getGradesFromAsignatura() {
        List<NotaCalculator> notas = new ArrayList<>();
        NotaCalculator n1 = new NotaCalculator();
        n1.setId(1);
        n1.setNombre("Nota 1");
        n1.setConocida(true);
        n1.setTeorico(true);
        n1.setValor(40);
        n1.setPorcentaje(30);

        NotaCalculator n2 = new NotaCalculator();
        n2.setId(1);
        n2.setNombre("Nota 2");
        n2.setTeorico(true);
        n2.setConocida(true);
        n2.setValor(30);
        n2.setPorcentaje(30);

        NotaCalculator n3 = new NotaCalculator();
        n3.setId(1);
        n3.setNombre("Nota 3");
        n3.setTeorico(true);
        n3.setConocida(false);
        n3.setValor(10);
        n3.setPorcentaje(40);

        notas.add(n1); notas.add(n2); notas.add(n3);

        Map<String,Object> actual = calculadora.getGradesFromAsignatura(notas,100,0,true);

        Map<String,Object> expected = new HashMap<>();

        expected.put("notasConocidas",getNotasConocidas());
        expected.put("message","Aprobable");
        expected.put("notasNoConocidas",getNotasNoConocidas());

        actual.forEach((key,value) -> {
            System.out.println("--------------"+key+"---------------");
            if (Objects.equals(key, "message")){
                System.out.println(value);
            }else{
                List<NotaCalculator> notasResponse = (List<NotaCalculator>) value;
                notasResponse.forEach( n ->{
                    System.out.println(n.getNombre() + " " + n.getValor());
                });
            }
        });
       // Mensaje de que ramo es aprobable
       Assertions.assertEquals(expected.get("message"),actual.get("message"));

       List<NotaCalculator> notasConocidasActual = (List<NotaCalculator>) actual.get("notasConocidas");
       List<NotaCalculator> notasConocidasExpected = (List<NotaCalculator>) getNotasConocidas();

       List<NotaCalculator> notasNoConocidasActual = (List<NotaCalculator>) actual.get("notasNoConocidas");
       List<NotaCalculator> notasNoConocidasExpected = (List<NotaCalculator>) getNotasNoConocidas();

       //Verificar valor de cada nota conocida se mantenga
       for (int i = 0; i < notasConocidasActual.size(); i++) {
           Assertions.assertEquals(notasConocidasExpected.get(i).getValor(),notasConocidasActual.get(i).getValor());
       }

       //Verificar valor de cada nota no conocida corresponda para que promedio sea 39.5
       for (int i = 0; i < notasNoConocidasActual.size(); i++) {
           Assertions.assertEquals(notasNoConocidasExpected.get(i).getValor(),notasNoConocidasActual.get(i).getValor());
       }

    }

    @Test
    @DisplayName("Asignatura Aprueban Juntos  | Asignatura NO Aprobable ")
    void getGradesFromAsignaturaNoAprobable() {
        List<NotaCalculator> notas = new ArrayList<>();
        NotaCalculator n1 = new NotaCalculator();
        n1.setId(1);
        n1.setNombre("Nota 1");
        n1.setConocida(true);
        n1.setTeorico(true);
        n1.setValor(15);
        n1.setPorcentaje(30);

        NotaCalculator n2 = new NotaCalculator();
        n2.setId(2);
        n2.setNombre("Nota 2");
        n2.setTeorico(true);
        n2.setConocida(true);
        n2.setValor(10);
        n2.setPorcentaje(30);

        NotaCalculator n3 = new NotaCalculator();
        n3.setId(3);
        n3.setNombre("Nota 3");
        n3.setTeorico(true);
        n3.setConocida(false);
        n3.setValor(10);
        n3.setPorcentaje(40);

        notas.add(n1); notas.add(n2); notas.add(n3);

        Map<String,Object> actual = calculadora.getGradesFromAsignatura(notas,100,0,true);

        Map<String,Object> expected = new HashMap<>();

        expected.put("notasConocidas",getNotasConocidas());
        expected.put("message","No Aprobable");
        expected.put("notasNoConocidas",getNotasNoConocidas());

        actual.forEach((key,value) -> {
            System.out.println("--------------"+key+"---------------");
            if (Objects.equals(key, "message")){
                System.out.println(value);
            }else{
                List<NotaCalculator> notasResponse = (List<NotaCalculator>) value;
                notasResponse.forEach( n ->{
                    System.out.println(n.getNombre() + " " + n.getValor());
                });
            }
        });
        // Mensaje de que ramo es NO es probable
        Assertions.assertEquals(expected.get("message"),actual.get("message"));
    }

    @Test
    @DisplayName("No hay datos en el sistema")
    void errorNoHayDatos(){
        Map<String, Object> actual = calculadora.getGradesFromAsignatura(new ArrayList<>(),100,0,true);

        actual.forEach((key,value) -> {
            System.out.println("--------------"+key+"---------------");
            if (Objects.equals(key, "message")){
                System.out.println(value);
            }else{
                List<NotaCalculator> notasResponse = (List<NotaCalculator>) value;
                notasResponse.forEach( n ->{
                    System.out.println(n.getNombre() + " " + n.getValor());
                });
            }
        });

        // Respuesta no sea nula
        assertNotNull(actual);
        // No lance alguna excepcion (que haga terminar la ejecucion del programa)
        assertDoesNotThrow(() -> calculadora.getGradesFromAsignatura(new ArrayList<>(),100,0,true));
    }

    private Object getNotasNoConocidas() {
        List<NotaCalculator> notasNoConocidas = new ArrayList<>();
        NotaCalculator n3 = new NotaCalculator();
        n3.setId(1);
        n3.setNombre("Nota 3");
        n3.setTeorico(true);
        n3.setConocida(false);
        n3.setValor(47);
        n3.setPorcentaje(40);

        notasNoConocidas.add(n3);
        return notasNoConocidas;
    }

    private Object getNotasNoConocidasSeparadas() {
        List<NotaCalculator> notasNoConocidas = new ArrayList<>();
        NotaCalculator n2 = new NotaCalculator();
        n2.setId(2);
        n2.setNombre("Nota 2");
        n2.setTeorico(true);
        n2.setConocida(false);
        n2.setValor(44);
        n2.setPorcentaje(70);

        NotaCalculator n3 = new NotaCalculator();
        n3.setId(3);
        n3.setNombre("Nota 3");
        n3.setTeorico(true);
        n3.setConocida(false);
        n3.setValor(40);
        n3.setPorcentaje(100);

        notasNoConocidas.add(n2);
        notasNoConocidas.add(n3);
        return notasNoConocidas;
    }

    private Object getNotasConocidas() {
        List<NotaCalculator> notasConocidas = new ArrayList<>();
        NotaCalculator n1 = new NotaCalculator();
        n1.setId(1);
        n1.setNombre("Nota 1");
        n1.setConocida(true);
        n1.setTeorico(true);
        n1.setValor(40);
        n1.setPorcentaje(30);

        NotaCalculator n2 = new NotaCalculator();
        n2.setId(1);
        n2.setNombre("Nota 2");
        n2.setTeorico(true);
        n2.setConocida(true);
        n2.setValor(30);
        n2.setPorcentaje(30);

        notasConocidas.add(n1);
        notasConocidas.add(n2);
        return notasConocidas;
    }

    private Object getNotasConocidasSeparadas() {
        List<NotaCalculator> notasConocidas = new ArrayList<>();
        NotaCalculator n1 = new NotaCalculator();
        n1.setId(1);
        n1.setNombre("Nota 1");
        n1.setConocida(true);
        n1.setTeorico(true);
        n1.setValor(30);
        n1.setPorcentaje(30);

        notasConocidas.add(n1);
        return notasConocidas;
    }
}