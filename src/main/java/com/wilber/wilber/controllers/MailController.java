package com.wilber.wilber.controllers;

import com.wilber.wilber.services.EmailServices;
import com.wilber.wilber.utils.LoggerWilber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class MailController {

    @Autowired
    private EmailServices emailServices;

    @Scheduled(fixedDelay = 30000)
    public void sendReminders(){
        LoggerWilber logger = new LoggerWilber();
        Thread thread = new Thread(() ->{
            try {
                emailServices.sendReminders();
                logger.createInfoLogs("Enviar Correos Recordatorios","Email Service","Local",this.getClass().getName());
            }catch (Exception e){
                logger.createWarningLogs(e.getMessage(),"Email Service","Local",this.getClass().getName());
            }
        });
        thread.start();
    }

}
