package com.wilber.wilber.controllers;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin
@RestController
public class DefaultController {

    @GetMapping
    public String defaultMethod(){
        return "<h1>API WILBER</h1>";
    }
}
