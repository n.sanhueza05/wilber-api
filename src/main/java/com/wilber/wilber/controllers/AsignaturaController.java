package com.wilber.wilber.controllers;

import com.wilber.wilber.models.Asignatura;
import com.wilber.wilber.models.AsignaturaInscrita;
import com.wilber.wilber.repositories.UsuarioRepository;
import com.wilber.wilber.services.AsignaturaServices;
import com.wilber.wilber.services.CarreraServices;
import com.wilber.wilber.utils.JwtUtils;
import com.wilber.wilber.utils.LoggerWilber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RequestMapping("/api/asignaturas/")
@CrossOrigin
@RestController
public class AsignaturaController {

    @Autowired
    private AsignaturaServices asignaturaServices;

    @Autowired
    private CarreraServices carreraServices;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private JwtUtils jwtUtils;

    private final LoggerWilber logger = new LoggerWilber();

    // Metodos admin
    // Asignaturas X Carrera
    @GetMapping("carrera")
    public ResponseEntity<Object> getAllAsignaturasByCarrera(@Param("id") long id){
        try{
            List<Asignatura> asignaturas = asignaturaServices.asignaturasByCarrera(id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(asignaturas);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),null,null,this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    // Agregar Asignatura carrera
    @PutMapping("carrera")
    public ResponseEntity<Object> addAsignaturaCarrera(@RequestHeader Map<String,String> headers,@RequestBody Asignatura asignatura, @Param("id") long id){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            asignaturaServices.addAsignatura(asignatura,id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body("Realizado Correctamente");
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    // Editar Asignatura Carrera
    @PostMapping("carrera")
    public ResponseEntity<String> editAsignaturaCarrera(@RequestHeader Map<String,String> headers,@RequestBody Asignatura asignatura){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            asignaturaServices.editAsignatura(asignatura);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body("Realizado Correctamente");
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    // Eliminar Asignatura carrera
    @DeleteMapping("carrera")
    public ResponseEntity<Object> deleteAsignaturaCarrera(@RequestHeader Map<String,String> headers,@RequestBody Asignatura asignatura){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            asignaturaServices.deleteAsignatura(asignatura);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body("Realizado Correctamente");
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }


    // Metodos Usuario

    //Agregar AsignaturaInscrita Usuario
    @PutMapping("usuario")
    public ResponseEntity<String> addAsignaturaUsuario(@RequestHeader Map<String,String> headers,@RequestBody AsignaturaInscrita inscrita){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            asignaturaServices.addAsignaturaUser(inscrita,jwtUtils.idFromToken(token));
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body("Realizado Correctamente");
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    // Ver asignaturas Inscritas Activas de Usuario
    @GetMapping("usuario")
    public ResponseEntity<Object> getAsignaturasInscritasUsuario(@RequestHeader Map<String,String> headers){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            List<AsignaturaInscrita> inscritas = asignaturaServices.getAsignaturasInscritas(jwtUtils.idFromToken(token));
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(inscritas);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    // Rendimiento de X asignatura
    @GetMapping("getOverallPerformance")
    public ResponseEntity<Object> getOverallPerformanceUser(@RequestHeader Map<String,String> headers,@RequestParam("id") long idInscrita){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            Map<String,Object> body = asignaturaServices.getPerformanceByAsignatura(idInscrita);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @GetMapping("byUser")
    public ResponseEntity<Object> getAllAsignaturasByUser(@RequestHeader Map<String,String> headers){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            List<Asignatura> body = asignaturaServices.getAllAsignaturasFromUser(jwtUtils.idFromToken(token));
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @DeleteMapping("usuario")
    public ResponseEntity<Object> deleteAsignaturaInscritaUser(@RequestHeader Map<String,String> headers,@RequestParam("id") long id){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            asignaturaServices.deleteAsignaturaInscrita(id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body("Realizado Correctamente");
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

}
