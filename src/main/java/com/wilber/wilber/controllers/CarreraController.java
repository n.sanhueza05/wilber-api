package com.wilber.wilber.controllers;

import com.wilber.wilber.models.Carrera;
import com.wilber.wilber.services.CarreraServices;
import com.wilber.wilber.utils.JwtUtils;
import com.wilber.wilber.utils.LoggerWilber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api/carreras/")
public class CarreraController {

    @Autowired
    private CarreraServices carreraServices;

    @Autowired
    private JwtUtils jwtUtils;

    private final LoggerWilber logger = new LoggerWilber();

    @GetMapping
    public List<Carrera> getAllCarreras(){
        List<Carrera> carreras = carreraServices.getAllCarreras();
        carreras.forEach( c -> c.setAsignaturas(null));
        return carreras;
    }

    // Metodos admin
    @GetMapping("rendimiento")
    public ResponseEntity<Object> getOverallPerformance(@RequestHeader Map<String,String> headers,
                                                        @RequestParam(value = "id",defaultValue = "1") long idCarrera){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            Map<String,Object> body;
            body = carreraServices.getPerformance(idCarrera);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    //Rendimiento nivel
    @PostMapping("rendimiento")
    public ResponseEntity<Object> getPerformanceByNivel(@RequestHeader Map<String,String> headers,
                                                        @RequestParam(value = "nivel") int nivel,
                                                        @RequestBody Carrera carrera){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            Map<String,Object> body;
            body = carreraServices.getPerformanceByNivel(nivel,carrera);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }


}
