package com.wilber.wilber.controllers;

import com.wilber.wilber.models.Nota;
import com.wilber.wilber.repositories.NotaRepository;
import com.wilber.wilber.services.AsignaturaServices;
import com.wilber.wilber.services.NotaServices;
import com.wilber.wilber.utils.JwtUtils;
import com.wilber.wilber.utils.LoggerWilber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/api/notas/")
public class NotasController {

    @Autowired
    private NotaServices notaServices;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private NotaRepository notaRepository;

    @Autowired
    private AsignaturaServices asignaturaServices;

    private final LoggerWilber logger = new LoggerWilber();

    @PostMapping("editarNota")
    public ResponseEntity<Object> editNotaUser(@RequestHeader Map<String,String> headers, @RequestBody Nota nota){
        try{
            String token = headers.get("authorization");
            HttpHeaders rHeaders = jwtUtils.renewToken(token, false);
            String body = notaServices.editNota(nota);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @GetMapping("notasUsuario")
    public ResponseEntity<Object> notasUser (@RequestHeader Map<String,String> headers){
        try{
            String token = headers.get("authorization");
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            List<Nota> body = notaServices.getNotas(token);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e) {
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    // Notas de X asignatura
    @GetMapping("approve")
    public ResponseEntity<Object> approve(@RequestHeader Map<String,String> headers,@RequestParam("id") long idInscrita){
        String token = headers.get("authorization");
        try {
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            Map<String,Object> body = asignaturaServices.getGradesToApprove(idInscrita);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }




}
