package com.wilber.wilber.controllers;

import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.services.AsignaturaServices;
import com.wilber.wilber.services.UsuarioServices;
import com.wilber.wilber.utils.JwtUtils;
import com.wilber.wilber.utils.LoggerWilber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/user/")
public class UsuarioController {

    @Autowired
    private UsuarioServices usuarioServices;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private AsignaturaServices asignaturaServices;

    private final LoggerWilber logger = new LoggerWilber();

    @PostMapping("login")
    public String login(@RequestBody Usuario user) {
        return usuarioServices.logearEmailPassword(user);
    }

    @PostMapping("signup")
    public String register(@RequestBody Usuario usuario) throws Exception{
        return usuarioServices.userRegister(usuario);
    }

    @GetMapping("verify")
    public ResponseEntity<Boolean> verifyAccount(@Param("code") String code) {
        Usuario usuario = usuarioServices.findByVerificationCode(code);
        if (usuario == null || usuario.isActivado()){
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(false);
        }else{
            usuario.setCodigoVerificacion(null);
            usuario.setActivado(true);
            usuarioServices.saveUser(usuario);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(true);
    }
    @GetMapping("dashboardAdmin")
    public ResponseEntity<Object> countUsers(@RequestHeader Map<String,String> headers) throws IOException {
        String token = headers.get("authorization");
        try{
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            Map<String,Object> body = new HashMap<>();
            body.put("userCount",usuarioServices.countUsers());
            body.put("activos",usuarioServices.countActives());
            body.put("estudiantesActivos",usuarioServices.countStudents());
            body.put("asignaturasRegistradas",asignaturaServices.countAsignaturas());
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e) {
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @GetMapping("infoAdmin")
    public ResponseEntity<Object> infoUserAdmin(@RequestHeader Map<String,String> headers){
        try{
            String token = headers.get("authorization");
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            Map<String,Object> body = usuarioServices.getInfoAdmin(token);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e) {
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @GetMapping("infoUser")
    public ResponseEntity<Object> infoUser(@RequestHeader Map<String,String> headers){
        try{
            String token = headers.get("authorization");
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            Map<String,Object> body = usuarioServices.getInfoUser(token);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e) {
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }


    @PostMapping("cambiarDatosAdmin")
    public ResponseEntity<Object> changeDataAdmin(@RequestHeader Map<String,String> headers, @RequestBody Usuario user){
        try{
            String token = headers.get("authorization");
            Map<String,Object> data = usuarioServices.editInformation(user);
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(data.get("changes"));
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @PostMapping("cambiarDatosUsuario")
    public ResponseEntity<Object> changeDataUser(@RequestHeader Map<String,String> headers, @RequestBody Usuario user){
        try{
            String token = headers.get("authorization");
            Map<String,Object> data = usuarioServices.editInformation(user);
            HttpHeaders rHeaders = jwtUtils.renewToken(token,false);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(data.get("changes"));
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }



    @GetMapping("listAll")
    public ResponseEntity<Object> getUsers(@RequestHeader Map<String,String> headers){
        try{
            String token = headers.get("authorization");
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            List<Usuario> body = usuarioServices.obtenerTodosUsuarios();
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @GetMapping("delete")
    public ResponseEntity<Object> deleteUserById(@RequestHeader Map<String,String> headers,@Param("id") Long id){
        try{
            String token = headers.get("authorization");
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            usuarioServices.deleteUserById(id);
            List<Usuario> body = usuarioServices.obtenerTodosUsuarios();
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @PostMapping("addAdmin")
    public ResponseEntity<Object> addAdmin(@RequestHeader Map<String,String> headers, @RequestBody Usuario usuario){
        try{
            String token = headers.get("authorization");
            HttpHeaders rHeaders = jwtUtils.renewToken(token,true);
            usuario.setAdmin(true);
            usuario.setCarrera(null);
            usuario.setActivado(true);
            String result = usuarioServices.addAdmin(usuario);
            Map<String,Object> body = new HashMap<>();
            List<Usuario> usuarios = usuarioServices.obtenerTodosUsuarios();
            body.put("usuarios",usuarios);
            body.put("status",result);
            return ResponseEntity.status(HttpStatus.ACCEPTED).headers(rHeaders).body(body);
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),headers.get("host"),headers.get("user-agent"),this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @GetMapping("recuperarContrasenna")
    public ResponseEntity<String> restorePassword(@Param("correo") String correo){
        try {
            usuarioServices.restorePassword(correo.toLowerCase());
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        }catch (Exception e){
            logger.createWarningLogs(e.getMessage(),null,null,this.getClass().getName());
            return ResponseEntity.badRequest().body("Error en la solicitud");
        }
    }

    @GetMapping("resetSendMail")
    public ResponseEntity<String> getCorreoResetPassword(@Param("code")String code){
        Usuario usuario = usuarioServices.findByVerificationCode(code);
        if (usuario == null || !usuario.isActivado()){
            return ResponseEntity.badRequest().build();
        }
        usuario.setCodigoVerificacion(null);
        usuarioServices.saveUser(usuario);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(jwtUtils.generateTokenFromUser(usuario));
    }



}
