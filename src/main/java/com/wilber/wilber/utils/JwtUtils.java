package com.wilber.wilber.utils;

import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.repositories.UsuarioRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Component
public class JwtUtils implements Serializable {

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public String generateTokenFromUser(Usuario usuario){
        return Jwts.builder()
                .claim("id",usuario.getId())
                .claim("usuario",usuario.getNombreUsuario())
                .claim("admin",usuario.isAdmin())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + (20 * 60 * 1000))) // 20 minutos
                .signWith(SignatureAlgorithm.HS256, TextCodec.BASE64.encode(secret)).compact();
    }
    public String getUserIdAdmin(String token, boolean admin) throws Exception {
        Claims claims = Jwts.parser()
            .setSigningKey(TextCodec.BASE64.encode(secret))
            .parseClaimsJws(token).getBody();
        if ((boolean)claims.get("admin") != admin){
            throw new Exception("Rol no corresponde");
        }
        return claims.get("id").toString();
    }

    public HttpHeaders renewToken(String tokenActual, boolean isAdmin) throws Exception {
        long id = Long.valueOf(getUserIdAdmin(tokenActual,isAdmin));
        Optional<Usuario> user = usuarioRepository.findById(id);
        if (user.isPresent()){
            HttpHeaders rHeaders = new HttpHeaders();
            rHeaders.set("Authorization",generateTokenFromUser(user.get()));
            rHeaders.set("Access-Control-Expose-Headers","Authorization");
            return rHeaders;
        }
        throw new Exception("Usuario no existe");
    }

    public long idFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(TextCodec.BASE64.encode(secret))
                .parseClaimsJws(token).getBody();
        return Long.valueOf(claims.get("id").toString());
    }

}
