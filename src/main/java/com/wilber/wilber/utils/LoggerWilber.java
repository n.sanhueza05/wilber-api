package com.wilber.wilber.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoggerWilber {

    final private Path path = Path.of("logs/log.txt");

    public void createWarningLogs(String message,String auth,String agent,String location) {
        if (auth == null) auth = "N/I";
        if (agent == null) agent = "N/I";
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String actualDate = formatter.format(date);
        try {
            path.toFile().getParentFile().mkdirs();
            path.toFile().createNewFile();
            FileWriter writer = new FileWriter(path.toFile(),true);
            writer.write(actualDate+" ERROR : \n");
            writer.write("AUTH: " +auth + " AGENT: "+agent +"\n");
            writer.write("LOCATION: "+location + "\n");
            writer.write(message + "\n");
            writer.write("------------------------------------------------------------\n");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println("Error al escribir en Disco");
            throw new RuntimeException(e);
        }

    }

    public void createInfoLogs(String message,String auth,String agent,String location) {
        if (auth == null) auth = "N/I";
        if (agent == null) agent = "N/I";
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String actualDate = formatter.format(date);
        try {
            path.toFile().getParentFile().mkdirs();
            path.toFile().createNewFile();
            FileWriter writer = new FileWriter(path.toFile(),true);
            writer.write(actualDate+" INFO : \n");
            writer.write("AUTH: " +auth + " AGENT: "+agent +"\n");
            writer.write("LOCATION: "+location + "\n");
            writer.write(message + "\n");
            writer.write("------------------------------------------------------------\n");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println("Error al escribir en Disco");
            throw new RuntimeException(e);
        }

    }

}
