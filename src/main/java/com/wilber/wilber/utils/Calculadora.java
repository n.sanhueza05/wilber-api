package com.wilber.wilber.utils;

import com.wilber.wilber.exceptions.NoAprobable;
import com.wilber.wilber.models.AsignaturaInscrita;
import com.wilber.wilber.models.Nota;
import com.wilber.wilber.models.Usuario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Calculadora {

    public float averageFromUser(Usuario u) {
        List<Nota> allNotas = new ArrayList<>();
        u.getAsignaturasInscritas().forEach( a -> allNotas.addAll(a.getNotas()));
        List<Nota> knowns = allNotas.stream().filter(Nota::isConocida).toList();
        float average = 0;
        for (Nota n : knowns){
            average += n.getValorNota();
        }
        average = (average/ knowns.size());
        return average;
    }


    public float averageAsignaturaInscrita(AsignaturaInscrita inscrita) {
        List<Nota> knowns = inscrita.getNotas().stream().filter(Nota::isConocida).toList();
        List<Integer> valNotas = new ArrayList<>();
        knowns.forEach( k ->{
            valNotas.add(k.getValorNota());
        });

        float average = 0;
        for(Integer i : valNotas){
            average += i;
        }
        average = (average/knowns.size());
        return average;
    }

    public float averagesAsignaturasInscritas(List<Float> averages) {
        float average = 0;
        for (Float a: averages) {
            average += a;
        }
        return (average/averages.size());
    }

    public Map<String,Object> getGradesFromAsignatura(List<NotaCalculator> notas,float porTeorico, float porPractico,boolean apruebaJunto) {
        if (apruebaJunto){
            return getGrades(notas,porTeorico,porPractico);
        }
        return getGradesSeparated(notas,porTeorico,porPractico);
    }

    private Map<String, Object> getGradesSeparated(List<NotaCalculator> notas, float porTeorico, float porPractico) {
        Map<String,Object> data = new HashMap<>();

        List<NotaCalculator> thGrades = notas.stream().filter(NotaCalculator::isTeorico).toList();
        List<NotaCalculator> prGrades = notas.stream().filter(n -> !n.isTeorico()).toList();

        List<NotaCalculator> thKnowns = thGrades.stream().filter(NotaCalculator::isConocida).toList();
        List<NotaCalculator> prKnowns = prGrades.stream().filter(NotaCalculator::isConocida).toList();
        List<NotaCalculator> thUnknowns = thGrades.stream().filter(n -> !n.isConocida()).toList();
        List<NotaCalculator> prUnknowns = prGrades.stream().filter(n -> !n.isConocida()).toList();
        float knownThAverage = getAverage(thKnowns,100,100);
        float knownPrAverage = getAverage(prKnowns,100,100);
        try {
            List<NotaCalculator> thEstimated = estimatedGrades(thUnknowns,100,porPractico,knownThAverage);
            List<NotaCalculator> prEstimated = estimatedGrades(prUnknowns,porTeorico,100,knownPrAverage);
            List<NotaCalculator> estimated = Stream.concat(thEstimated.stream(),prEstimated.stream()).toList();
            data.put("notasNoConocidas",estimated);
            data.put("message","Aprobable");
        }catch (NoAprobable e){
            List<NotaCalculator> estimated = Stream.concat(thUnknowns.stream(),prUnknowns.stream()).toList();
            data.put("notasNoConocidas",estimated);
            data.put("message","No Aprobable");
        }
        List<NotaCalculator> knowns = Stream.concat(thKnowns.stream(),prKnowns.stream()).toList();
        data.put("notasConocidas",knowns);
        return data;
    }

    private Map<String,Object> getGrades(List<NotaCalculator> notas, float porTeorico, float porPractico) {
        List<NotaCalculator> knowns = notas.stream().filter(NotaCalculator::isConocida).toList();
        List<NotaCalculator> unkowns = notas.stream().filter(n -> !n.isConocida()).toList();
        float averageKnowns = getAverage(knowns,porTeorico,porPractico);

        Map<String,Object> data = new HashMap<>();
        try {
            List<NotaCalculator> estimated = estimatedGrades(unkowns,porTeorico,porPractico,averageKnowns);
            data.put("notasNoConocidas",estimated);
            data.put("message","Aprobable");
        } catch (NoAprobable e) {
            data.put("notasNoConocidas",unkowns);
            data.put("message","No Aprobable");
        }
        data.put("notasConocidas",knowns);
        return data;
    }

    private List<NotaCalculator> estimatedGrades(List<NotaCalculator> unknowns, float porTeorico, float porPractico, float averageKnowns) throws NoAprobable {
        int actualLimit = 40;
        int gradeExceed = 71;

        if (unknowns.size() == 0 && averageKnowns < 39.5) throw new NoAprobable();
        else if(unknowns.size() == 0 && averageKnowns >= 39.5) return unknowns;

        unknowns.get(0).setValor(9);

        for (int i = 0; i < unknowns.size(); i++) {
            boolean approve = false;
            do {
                unknowns.get(i).setValor(unknowns.get(i).getValor()+1);
                float averageUnknowns = getAverage(unknowns,porTeorico,porPractico);
                if ((averageUnknowns + averageKnowns) >= 39.5) {
                    approve = true;
                    break;
                }
            }while (unknowns.get(i).getValor() < actualLimit);
            if (approve){
                break;
            }else if(i == (unknowns.size()-1)){
                i = -1;
                actualLimit += 5;
                if (actualLimit > gradeExceed){
                    throw new NoAprobable();
                }
            }
        }
        return unknowns;
    }

    private float getAverage(List<NotaCalculator> notas, float porTeorico, float porPractico) {
        float average = 0;
        for (NotaCalculator n:notas) {
            if (n.isTeorico()){
                average += n.getValor() * ((n.getPorcentaje() / 100) * (porTeorico / 100));
            }else{
                average += n.getValor() * ((n.getPorcentaje()/100) * (porPractico/100));
            }
        }
        return average;
    }

}
