package com.wilber.wilber.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class NotaCalculator {
    private long id;
    private String nombre;
    private int valor;
    private float porcentaje;
    private boolean conocida;
    private boolean teorico;
    private Date fechaCorreo;
}
