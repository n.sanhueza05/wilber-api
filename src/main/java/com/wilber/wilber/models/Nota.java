package com.wilber.wilber.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Nota {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private float porcentaje;

    @NotNull
    private boolean teorico;

    @NotNull
    private boolean conocida;

    @Nullable
    private Date fechaEvaluacion;

    @Nullable
    private Date fechaCorreo;

    @Nullable
    private boolean correoEnviado;

    @NotNull
    private String nombre;

    @Nullable
    private int valorNota;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "asignatura_inscrita_id")
    private AsignaturaInscrita asignaturaInscrita;

}
