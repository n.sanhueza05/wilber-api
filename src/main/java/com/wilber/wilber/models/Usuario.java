package com.wilber.wilber.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "usuarios")
@NoArgsConstructor
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre_usuario")
    private String nombreUsuario;
    @NotNull
    @Column(name = "email")
    private String email;
    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @Column(name = "admin")
    private boolean admin;

    @NotNull
    @Column(name = "activado")
    private boolean activado;

    @Nullable
    @Column(name = "codigo_verificacion",length = 64)
    private String codigoVerificacion;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "carrera_id",referencedColumnName = "id")
    private Carrera carrera;

    @Nullable
    @OneToMany(mappedBy = "usuario",cascade = CascadeType.ALL,orphanRemoval = true)
    @JsonIgnore
    private List<AsignaturaInscrita> asignaturasInscritas = new ArrayList<>();

}
