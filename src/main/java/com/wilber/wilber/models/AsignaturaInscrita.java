package com.wilber.wilber.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class AsignaturaInscrita {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private float porTeorico;

    @NotNull
    private float porPractico;

    @NotNull
    private boolean eliminado;

    @NotNull
    private boolean apruebanJuntos;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "asignatura_id")
    private Asignatura asignatura;

    @NotNull
    @OneToMany(mappedBy = "asignaturaInscrita",cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Nota> notas = new ArrayList<>();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;
}
