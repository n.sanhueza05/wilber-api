package com.wilber.wilber.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Carrera {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String nombre;

    @Nullable
    private String codigo;

    @JsonIgnore
    @OneToMany(mappedBy = "carrera",cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Asignatura> asignaturas = new ArrayList<>();

    public Carrera addAsignatura(Asignatura asignatura){
        asignaturas.add(asignatura);
        asignatura.setCarrera(this);
        return this;
    }

}
