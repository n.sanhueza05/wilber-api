package com.wilber.wilber.repositories;

import com.wilber.wilber.models.Carrera;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarreraRepository extends JpaRepository<Carrera,Long> {
}
