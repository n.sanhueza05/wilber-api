package com.wilber.wilber.repositories;

import com.wilber.wilber.models.Asignatura;
import com.wilber.wilber.models.Carrera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;

@EnableJpaRepositories
public interface AsignaturaRepository extends JpaRepository<Asignatura,Long> {
}
