package com.wilber.wilber.repositories;

import com.wilber.wilber.models.Carrera;
import com.wilber.wilber.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Long> {
    Optional<Usuario> findByEmail(String email);
    Usuario findByCodigoVerificacion(String codigo);
    long countByActivado(boolean activado);
    long countByCarreraNotNullAndActivadoIsTrue();

    List<Usuario> findByCarrera(Carrera carrera);

}
