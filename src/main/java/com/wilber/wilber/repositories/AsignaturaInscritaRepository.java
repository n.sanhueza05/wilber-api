package com.wilber.wilber.repositories;

import com.wilber.wilber.models.Asignatura;
import com.wilber.wilber.models.AsignaturaInscrita;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AsignaturaInscritaRepository extends JpaRepository<AsignaturaInscrita,Long> {
    List<AsignaturaInscrita> findByAsignatura(Asignatura a);
}
