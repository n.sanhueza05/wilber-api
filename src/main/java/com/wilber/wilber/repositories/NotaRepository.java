package com.wilber.wilber.repositories;

import com.wilber.wilber.models.AsignaturaInscrita;
import com.wilber.wilber.models.Nota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface NotaRepository extends JpaRepository<Nota,Long> {

    @Query("select a from Nota a where a.fechaCorreo <= :date")
    List<Nota> findAllBeforeDate(@Param("date") Date now);

    List<Nota> findByAsignaturaInscrita(AsignaturaInscrita inscrita);
}
