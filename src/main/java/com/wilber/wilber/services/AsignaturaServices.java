package com.wilber.wilber.services;

import com.wilber.wilber.models.*;
import com.wilber.wilber.repositories.*;
import com.wilber.wilber.utils.Calculadora;
import com.wilber.wilber.utils.NotaCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AsignaturaServices {

    private Calculadora calculadora = new Calculadora();

    @Autowired
    private AsignaturaInscritaRepository asignaturaInscritaRepository;

    @Autowired
    private AsignaturaRepository asignaturaRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private NotaRepository notaRepository;

    @Autowired
    private CarreraRepository carreraRepository;
    public long countAsignaturas() {
        return asignaturaInscritaRepository.count();
    }

    public List<Asignatura> asignaturasByCarrera(long id) throws Exception {
        Carrera c = getCarreraFromId(id);
        return c.getAsignaturas();
    }


    public void addAsignatura(Asignatura asignatura, long idCarrera) throws Exception {
        Carrera c = getCarreraFromId(idCarrera);
        asignatura.setCarrera(c);
        Asignatura a = asignaturaRepository.save(asignatura);
        c.addAsignatura(a);
        carreraRepository.save(c);
    }

    public void editAsignatura(Asignatura asignatura) throws Exception {
        Asignatura asignaturaDB = getAsignaturaInscritaById(asignatura.getId());
        asignaturaDB.setNombre(asignatura.getNombre());
        asignaturaDB.setNivel(asignatura.getNivel());
        asignaturaRepository.save(asignaturaDB);
    }

    private Asignatura getAsignaturaInscritaById(long id) throws Exception {
        Optional<Asignatura> a = asignaturaRepository.findById(id);
        if (a.isEmpty()) throw new Exception("Asignatura no existe");
        return a.get();
    }

    public void deleteAsignatura(Asignatura asignatura) throws Exception {
        asignatura = getAsignaturaInscritaById(asignatura.getId());
        List<AsignaturaInscrita> inscritas = asignaturaInscritaRepository.findByAsignatura(asignatura);
        inscritas.forEach( i -> {
            notaRepository.deleteAll(i.getNotas());
        });
        asignaturaInscritaRepository.deleteAll(inscritas);
        asignaturaRepository.deleteById(asignatura.getId());
    }

    public void addAsignaturaUser(AsignaturaInscrita inscrita, long userId) throws Exception {
        Usuario u = getUserById(userId);
        inscrita.setEliminado(false);
        notaRepository.saveAll(inscrita.getNotas());
        inscrita = asignaturaInscritaRepository.save(inscrita);
        AsignaturaInscrita finalInscrita = inscrita;
        inscrita.getNotas().forEach(n -> {
            n.setAsignaturaInscrita(finalInscrita);
        });
        notaRepository.saveAll(inscrita.getNotas());
        u.getAsignaturasInscritas().add(inscrita);
        inscrita.setUsuario(u);
        asignaturaInscritaRepository.save(inscrita);
        usuarioRepository.save(u);
    }

    public List<AsignaturaInscrita> getAsignaturasInscritas(long userId) throws Exception {
        Usuario u = getUserById(userId);
        List<AsignaturaInscrita> inscritas = u.getAsignaturasInscritas();
        inscritas = inscritas.stream().filter( i -> !i.isEliminado()).toList();
        return inscritas;
    }

    public Map<String, Object> getPerformanceByAsignatura(long idInscrita) throws Exception {
        Optional<AsignaturaInscrita> opInscrita = asignaturaInscritaRepository.findById(idInscrita);
        if (opInscrita.isEmpty()) throw new Exception("Asignatura Inscrita no Existe");
        AsignaturaInscrita inscrita = opInscrita.get();
        Asignatura asignatura = getAsignaturaById(inscrita.getAsignatura().getId());
        Map<String,Object> data = new HashMap<>();
        List<AsignaturaInscrita> inscritas = asignaturaInscritaRepository.findByAsignatura(asignatura);
        List<Float> averages = new ArrayList<>();
        inscritas.forEach(i -> averages.add(calculadora.averageAsignaturaInscrita(i)));

        Collections.sort(averages);
        float averageUser = calculadora.averageAsignaturaInscrita(inscrita);
        float averagePerfomance = calculadora.averagesAsignaturasInscritas(averages);
        data.put("promedioUsuario",averageUser);
        data.put("promedioBajo",averages.get(0));
        data.put("promedio",averagePerfomance);
        data.put("promedioAlto",averages.get(averages.size()-1));
        return data;
    }

    private Usuario getUserById(long userId) throws Exception {
        Optional<Usuario> user = usuarioRepository.findById(userId);
        if (user.isEmpty()) throw new Exception("Usuario no existe");
        return user.get();
    }

    private Asignatura getAsignaturaById(long asignaturaId) throws Exception {
        Optional<Asignatura> asignatura = asignaturaRepository.findById(asignaturaId);
        if (asignatura.isEmpty()) throw new Exception("Asignatura no existe");
        return asignatura.get();
    }

    private Carrera getCarreraFromId(long id) throws Exception {
        Optional<Carrera> carrera = carreraRepository.findById(id);
        if (carrera.isEmpty()) throw new Exception("Carrera no existe");
        return carrera.get();
    }

    public Map<String, Object> getGradesToApprove(long idInscrita) throws Exception {
        Optional<AsignaturaInscrita> inscrita = asignaturaInscritaRepository.findById(idInscrita);
        if (inscrita.isEmpty()) throw new Exception("Asignatura Inscrita no existe");
        AsignaturaInscrita i = inscrita.get();
        List<Nota> notas = notaRepository.findByAsignaturaInscrita(i);
        List<NotaCalculator> calculators = new ArrayList<>();

        notas.forEach(n ->{
            NotaCalculator nc = new NotaCalculator();
            nc.setId(n.getId());
            nc.setNombre(n.getNombre());
            nc.setValor(n.getValorNota());
            nc.setPorcentaje(n.getPorcentaje());
            nc.setConocida(n.isConocida());
            nc.setTeorico(n.isTeorico());
            nc.setFechaCorreo(n.getFechaCorreo());
            calculators.add(nc);
        });

        return calculadora.getGradesFromAsignatura(calculators,i.getPorTeorico(),i.getPorPractico(),i.isApruebanJuntos());
    }

    public List<Asignatura> getAllAsignaturasFromUser(long idFromToken) throws Exception {
        Usuario u = getUserById(idFromToken);
        return u.getCarrera().getAsignaturas();
    }

    public void deleteAsignaturaInscrita(long id) throws Exception {
        Optional<AsignaturaInscrita> inscrita = asignaturaInscritaRepository.findById(id);
        if (inscrita.isEmpty()) throw new Exception("Asignatura Inscrita No Existe");
        inscrita.get().setEliminado(true);
        asignaturaInscritaRepository.save(inscrita.get());
    }
}
