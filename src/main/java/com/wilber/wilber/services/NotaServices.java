package com.wilber.wilber.services;

import com.wilber.wilber.models.Nota;
import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.repositories.NotaRepository;
import com.wilber.wilber.repositories.UsuarioRepository;
import com.wilber.wilber.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NotaServices {

    @Autowired
    private NotaRepository notaRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private JwtUtils jwtUtils;


    public String editNota(Nota notaModificada) throws Exception {
        Optional<Nota> nota = notaRepository.findById(notaModificada.getId());
        if(nota.isEmpty()) throw new Exception("Nota no encontrada");
        Nota notabd = nota.get();
        notabd.setNombre(notaModificada.getNombre());
        notabd.setConocida(notaModificada.isConocida());
        notabd.setFechaCorreo(notaModificada.getFechaCorreo());
        if(!notabd.isConocida()) notabd.setValorNota(10);
        else notabd.setValorNota(notaModificada.getValorNota());
        notaRepository.save(notabd);
        return "Modificacion correcta";
    }

    public List<Nota> getNotas(String token) throws Exception {
        long id = jwtUtils.idFromToken(token);
        Optional<Usuario> usuario = usuarioRepository.findById(id);
        List<Nota> notasUsuario = new ArrayList<>();

        usuario.get().getAsignaturasInscritas().forEach(asignaturaInscrita -> {
            if (!asignaturaInscrita.isEliminado()) {
                notasUsuario.addAll(asignaturaInscrita.getNotas());
            }
        });
        return notasUsuario;
    }

}
