package com.wilber.wilber.services;

import com.wilber.wilber.models.Nota;
import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.repositories.NotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class EmailServices{

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;

    @Autowired
    private NotaRepository notaRepository;

    final String URL = "https://wilberapp.herokuapp.com/";

    public void sendVerificationCode(Usuario usuario) throws MessagingException, IOException {
        MimeMessage mailMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage);
        helper.setFrom(sender);
        helper.setTo(usuario.getEmail());
        helper.setSubject("Por favor, verifica tu cuenta Wilber...");
        Path path = Path.of("src/main/java/com/wilber/wilber/emailMessages/verifyAccount.html");
        String content = Files.readString(path);
        content = content.replace("[[nombre]]",usuario.getNombreUsuario());
        String verifyURL = URL +"completed" + usuario.getCodigoVerificacion();
        content = content.replace("[[URL]]",verifyURL);
        helper.setText(content,true);
        javaMailSender.send(mailMessage);
    }

    public void sendRestorePassword(Usuario usuario) throws MessagingException, IOException {
        MimeMessage mailMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage);
        helper.setFrom(sender);
        helper.setTo(usuario.getEmail());
        helper.setSubject("Solicitud recuperación contraseña Wilber");
        Path path = Path.of("src/main/java/com/wilber/wilber/emailMessages/restorePassword.html");
        String content = Files.readString(path);
        content = content.replace("[[nombre]]",usuario.getNombreUsuario());
        String verifyURL = URL + "resetPassword" +usuario.getCodigoVerificacion();
        content = content.replace("[[URL]]",verifyURL);
        helper.setText(content,true);
        javaMailSender.send(mailMessage);
    }


    @Transactional
    public List<Nota> sendReminders() throws MessagingException, IOException {
        List<Nota> notas = notaRepository.findAllBeforeDate(new Date());
        for (Nota n : notas){
            MimeMessage mailMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mailMessage);
            helper.setFrom(sender);
            helper.setTo(n.getAsignaturaInscrita().getUsuario().getEmail());
            helper.setSubject("Recordatorio Evaluación "+n.getNombre());
            Path path = Path.of("src/main/java/com/wilber/wilber/emailMessages/reminder.html");
            String content = Files.readString(path);
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            String dateFormated = format.format(n.getFechaCorreo());
            content = content.replace("[[fechaEvaluacion]]",dateFormated);
            float percentage = (n.getPorcentaje()/100);
            if (n.isTeorico()){
                percentage *= n.getAsignaturaInscrita().getPorTeorico();
            }else {
                percentage *= n.getAsignaturaInscrita().getPorPractico();
            }
            content = content.replace("[[evaluacion]]",n.getNombre());
            content = content.replace("[[porcentaje]]",String.valueOf(percentage));
            helper.setText(content,true);
            n.setFechaCorreo(null);
            n.setCorreoEnviado(true);
            notaRepository.save(n);
            javaMailSender.send(mailMessage);
        }
        return notas;
    }

}
