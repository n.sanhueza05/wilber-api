package com.wilber.wilber.services;

import com.wilber.wilber.models.Asignatura;
import com.wilber.wilber.models.AsignaturaInscrita;
import com.wilber.wilber.models.Carrera;
import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.repositories.AsignaturaInscritaRepository;
import com.wilber.wilber.repositories.CarreraRepository;
import com.wilber.wilber.repositories.UsuarioRepository;
import com.wilber.wilber.utils.Calculadora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class CarreraServices {
    @Autowired
    private CarreraRepository carreraRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private AsignaturaInscritaRepository asignaturaInscritaRepository;

    private final Calculadora calculadora = new Calculadora();

    public List<Carrera> getAllCarreras() {
        return carreraRepository.findAll();
    }


    public Map<String,Object> getPerformance(long idCarrera) throws Exception {
        Carrera c = getCarreraById(idCarrera);
        List<Usuario> users = usuarioRepository.findByCarrera(c);
        Map<String,Object> data = new HashMap<>();
        List<Float> promedios = new ArrayList<>();
        for (Usuario u: users) {
            promedios.add(calculadora.averageFromUser(u));
        }
        Collections.sort(promedios);
        float averageCarrera = 0;
        for(Float a : promedios){
            averageCarrera += a;
        }
        averageCarrera = averageCarrera/ promedios.size();
        data.put("promedioBajo",promedios.get(0));
        data.put("promedio",averageCarrera);
        data.put("promedioAlto",promedios.get(promedios.size()-1));
        return data;
    }

    public Map<String, Object> getPerformanceByNivel(int nivel, Carrera carrera) throws Exception {
        Carrera c = getCarreraById(carrera.getId());
        List<Asignatura> asignaturas = c.getAsignaturas().stream().filter(a -> a.getNivel() == nivel).toList();
        List<AsignaturaInscrita> inscritas = new ArrayList<>();
        asignaturas.forEach(a -> {
            inscritas.addAll(asignaturaInscritaRepository.findByAsignatura(a));
        });

        List<Float> averages = new ArrayList<>();
        for (AsignaturaInscrita i: inscritas) {
            averages.add(calculadora.averageAsignaturaInscrita(i));
        }
        Collections.sort(averages);

        float average = 0;
        for (Float a : averages){
            average += a;
        }
        average = (average/averages.size());


        Map<String,Object> data = new HashMap<>();
        data.put("promedioBajo",averages.get(0));
        data.put("promedio",average);
        data.put("promedioAlto",averages.get(averages.size()-1));
        return data;
    }

    private Carrera getCarreraById(long id) throws Exception {
        Optional<Carrera> optional = carreraRepository.findById(id);
        if (optional.isEmpty()) throw new Exception("Carrera no encontrada");
        return optional.get();
    }
}
