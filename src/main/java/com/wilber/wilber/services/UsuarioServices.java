package com.wilber.wilber.services;

import com.wilber.wilber.models.Usuario;
import com.wilber.wilber.repositories.UsuarioRepository;
import com.wilber.wilber.utils.JwtUtils;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;


@Service
public class UsuarioServices {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private EmailServices emailServices;


    public String generateToken(Usuario usuario){
        return jwtUtils.generateTokenFromUser(usuario);
    }

    public String logearEmailPassword(Usuario user) {
        user.setEmail(user.getEmail().toLowerCase());
        Optional<Usuario> compare = usuarioRepository.findByEmail(user.getEmail());
        if(compare.isPresent()){
            if(!compare.get().isActivado()){
                return "Usuario no activado";
            }
            if (bCryptPasswordEncoder.matches(user.getPassword(),compare.get().getPassword())){
                return generateToken(compare.get());
            }else{
                return "Contraseña Incorrecta";
            }
        }
        return "Usuario inexistente";
    }

    public String userRegister(Usuario usuario) throws Exception {
        usuario.setEmail(usuario.getEmail().toLowerCase());
        Optional<Usuario> compare = usuarioRepository.findByEmail(usuario.getEmail());
        String code = RandomString.make(20);
        usuario.setCodigoVerificacion(code);
        usuario.setAdmin(false);
        usuario.setPassword(bCryptPasswordEncoder.encode(usuario.getPassword()));
        if (compare.isEmpty()){
            Thread t = new Thread(() ->{
                try {
                    emailServices.sendVerificationCode(usuario);
                } catch (MessagingException | IOException e) {
                    throw new RuntimeException(e);
                }
            });
            t.start();
            usuario.setActivado(false);
            usuarioRepository.save(usuario);
            return "Registrado correctamente";
        }
        return "Email en Uso";
    }

    public Usuario findByVerificationCode(String code) {
        return usuarioRepository.findByCodigoVerificacion(code);
    }

    public void saveUser(Usuario usuario){
        usuarioRepository.save(usuario);
    }

    public long countUsers() {
        return usuarioRepository.count();
    }

    public long countActives(){
        return usuarioRepository.countByActivado(true);
    }

    public long countStudents() {
        return usuarioRepository.countByCarreraNotNullAndActivadoIsTrue();
    }

    public Map<String,Object> getInfoAdmin(String token) throws Exception {
        long id = jwtUtils.idFromToken(token);
        Optional<Usuario> user = usuarioRepository.findById(id);
        if (user.isPresent()){
            Map<String,Object> body = new HashMap<>();
            body.put("userName",user.get().getNombreUsuario());
            body.put("correoElectronico",user.get().getEmail());
            return body;
        }
        throw new Exception("Usuario no encontrado");
    }


    public Map<String,Object> editInformation(Usuario user) {
        Optional<Usuario> usuario = usuarioRepository.findByEmail(user.getEmail());
        String changes = "";
        Map<String,Object> data = new HashMap<>();
        if (user.getNombreUsuario().length() > 3 ){
            usuario.get().setNombreUsuario(user.getNombreUsuario());
            changes += ("Nombre de Usuario cambiado exitosamente\n");
        }
        if (user.getPassword().length() > 4){
            usuario.get().setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            changes += ("Contraseña cambiada exitosamente");
        }
        if (!usuario.get().isAdmin() && (usuario.get().getCarrera().getId() != user.getCarrera().getId())){
            usuario.get().setCarrera(user.getCarrera());
            changes += ("Carrera Modificada");
        }
        Usuario u = usuarioRepository.save(usuario.get());
        data.put("user",u);
        data.put("changes",changes);
        return data;
    }


    public List<Usuario> obtenerTodosUsuarios() {
        return usuarioRepository.findAll();
    }

    public void deleteUserById(Long id) {
        usuarioRepository.deleteById(id);
    }

    public String addAdmin(Usuario usuario) {
        Optional<Usuario> compare = usuarioRepository.findByEmail(usuario.getEmail());
        if (compare.isPresent()){
            return "Email en uso";
        }
        usuario.setPassword(bCryptPasswordEncoder.encode(usuario.getPassword()));
        usuarioRepository.save(usuario);
        return "Completado";
    }

    public void restorePassword(String correo) throws Exception {
        Optional<Usuario> usuario = usuarioRepository.findByEmail(correo);
        if(usuario.isEmpty()) throw new Exception("Correo no se encuentra registrado");
        String code = RandomString.make(20);
        boolean flag = false;
        do {
            Usuario user = usuarioRepository.findByCodigoVerificacion(code);
            if (user == null){
                usuario.get().setCodigoVerificacion(code);
                usuarioRepository.save(usuario.get());
                Thread t = new Thread(() ->{
                    try {
                        emailServices.sendRestorePassword(usuario.get());
                    } catch (Exception e) {
                        throw new RuntimeException("No se ha podido enviar el correo");
                    }
                });
                t.start();
                flag = true;
            }else {
                code = RandomString.make(20);
            }
        }while (!flag);
    }

    public Map<String, Object> getInfoUser(String token) throws Exception {
        long id = jwtUtils.idFromToken(token);
        Optional<Usuario> user = usuarioRepository.findById(id);
        if (user.isPresent()){
            Map<String,Object> body = new HashMap<>();
            body.put("userName",user.get().getNombreUsuario());
            body.put("correoElectronico",user.get().getEmail());
            body.put("carrera",user.get().getCarrera());
            return body;
        }
        throw new Exception("Usuario no encontrado");
    }
}
